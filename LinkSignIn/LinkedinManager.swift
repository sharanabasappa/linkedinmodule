//
//  LinkedinManager.swift
//  LinkSignIn
//
//  Created by Fresher on 07/04/17.
//  Copyright © 2017 Fresher. All rights reserved.
//

import Foundation
import UIKit
typealias completeHandler = ((_ succeeded: Bool, _ error : LISDKAPIError?) -> ())



/*
 
 */
class LinkedInManager {
    
   
    
   class func stringJson(text : String) ->[String:Any]? {
        
        if let data = text.data(using: .utf8) {
            do{
                let json = try JSONSerialization.jsonObject(with:data , options: []) as! [String : Any]
                
                return json
                
            }
            catch let erroe as NSError {
                debugPrint(erroe)
            }
        }
        
        
        return nil
    }
    
    class func CreateLoginSeesion( successClosure : @escaping completeHandler){
        LISDKSessionManager.createSession(withAuth: [LISDK_BASIC_PROFILE_PERMISSION,LISDK_W_SHARE_PERMISSION], state: nil, showGoToAppStoreDialog: true, successBlock: { (response) in
            
            successClosure(true, nil)
           

            
        }) { (error) in
          successClosure(false, error as? LISDKAPIError)
        }
    }
    
    class func getBasicInformation(){
      
        let url = "https://api.linkedin.com/v1/people/~?format=json"
        if (LISDKSessionManager.hasValidSession()){
            LISDKAPIHelper.sharedInstance().getRequest(url, success: { (response) in
                 LinkidInBasicinfo.sharedInstance.setCompleteInfo(basicInfo: LinkedInManager.stringJson(text : (response?.data)!)!)
                
            }, error: { (error) in
                
            })
        }
        
    }
    
    class func getCompleteInformation(successClosure : @escaping completeHandler){
        if (LISDKSessionManager.hasValidSession()){

//            let shareInfoList = ["id","first-name","last-name","maiden-name","formatted-name","phonetic-first-name","phonetic-last-name","headline","location","industry","current-share","num-connections","num-connections-capped","summary","specialties","specialties","picture-url","picture-urls::(original)","site-standard-profile-request","api-standard-profile-request","api-standard-profile-request","email-address"]
            let url = "https://api.linkedin.com/v1/people/~:(location,siteStandardProfileRequest,industry,num-connections,picture-urls::(original),id,first-name,last-name,summary,positions,headline)?format=json"
            
            LISDKAPIHelper.sharedInstance().getRequest(url, success: { (response) in
                
                 LinkidInBasicinfo.sharedInstance.setCompleteInfo(basicInfo: LinkedInManager.stringJson(text : (response?.data)!)!)
                 successClosure(true,nil)
                
            }, error: { (error) in
               successClosure(true,error)
            })
            
        }
       
    }
    class func getContactsInformation(successClosure : @escaping completeHandler){
//        LISDKSessionManager.createSession(withAuth: [LISDK_CONTACT_INFO_PERMISSION], state: nil, showGoToAppStoreDialog: true, successBlock: { (response) in
//            
//           
//            
//            
//            
//        }) { (error) in
//                   }
        if (LISDKSessionManager.hasValidSession()){
            
            //            let shareInfoList = ["id","first-name","last-name","maiden-name","formatted-name","phonetic-first-name","phonetic-last-name","headline","location","industry","current-share","num-connections","num-connections-capped","summary","specialties","specialties","picture-url","picture-urls::(original)","site-standard-profile-request","api-standard-profile-request","api-standard-profile-request","email-address"]
            let url = "https://api.linkedin.com/v1/people/~:(phone-numbers,bound-account-types,im-accounts,main-address,twitter-accounts)?format=json"
            
            LISDKAPIHelper.sharedInstance().getRequest(url, success: { (response) in
                
                LinkidInBasicinfo.sharedInstance.setCompleteInfo(basicInfo: LinkedInManager.stringJson(text : (response?.data)!)!)
                successClosure(true,nil)
                
            }, error: { (error) in
                successClosure(true,error)
            })
            
        }
        
    }
    
  class  func shareInfo(successClosure : @escaping completeHandler)
    {
        let session = LISDKSessionManager.sharedInstance().session
        if (session?.isValid())!{
            let url = "https://api.linkedin.com/v1/people/~/shares?format=json"
            /*{
             "comment": "Check out developer.linkedin.com!",
             "content": {
             "title": "LinkedIn Developers Resources",
             "description": "Leverage LinkedIn's APIs to maximize engagement",
             "submitted-url": "https://developer.linkedin.com",
             "submitted-image-url": "https://example.com/logo.png"
             },
             "visibility": {
             "code": "anyone"
             }
             
             }
             */
            
            let model = LinkShareModel()
            model.comment = "Check out developer.linkedin.com!!!"
            model.content.title = "LinkedInevelopers Resources!!"
            
            model.content.submitted_url =  "https://developer.linkedin.com/sl122"
            model.content.submitted_image_url = "https://example.com/logo.png"
            model.content.desciption  = "Leverage LinkedIn's APIs to maximize engagement"
            model.visibility.code = Visiblity.Anyone.rawValue
            let payload: String =  "{\"comment\":\"Check out developer.linkedin.com! http://linkd.in/1FC2PyG\",\"visibility\":{ \"code\":\"anyone\" }}"

            LISDKAPIHelper.sharedInstance().postRequest(url, stringBody:payload, success: {
                response in
                  successClosure(true, nil)
                
            }, error: {
                error in
                //Do something with the error
                successClosure(false, error)
                print("\(error?.description)")
            })
            
            
            
        }
        
    }
    
    class  func checkFornil(value:Any?) -> Any?{
        if let value = value
        {
            return value
        }
        return ""
    }

}

//
//  InfoShareViewController.swift
//  LinkSignIn
//
//  Created by Fresher on 07/04/17.
//  Copyright © 2017 Fresher. All rights reserved.
//

import UIKit

class InfoShareViewController: UIViewController,UICollectionViewDelegateFlowLayout,UICollectionViewDataSource {
    
    let shareInfoList = ["id","first-name","last-name","maiden-name","formatted-name","phonetic-first-name","phonetic-last-name","headline","location","industry","current-share","num-connections","num-connections-capped","summary","specialties","specialties","picture-url","picture-urls::(original)","site-standard-profile-request","api-standard-profile-request","api-standard-profile-request","email-address"]
    var isSelectedItemList : [Bool] = []
    
    @IBOutlet weak var shareInfoCollectionView: UICollectionView?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        resetTheList()
        let nib = UINib(nibName: "infoSelectedCell", bundle: nil)
        shareInfoCollectionView?.register(nib, forCellWithReuseIdentifier: "infoSelectedCell")
        shareInfoCollectionView?.dataSource = self
        shareInfoCollectionView?.delegate = self
        // Do any additional setup after loading the view.
    }
    
    func resetTheList(){
        var count = 0
        while count < shareInfoList.count {
            isSelectedItemList.append(false)
            count = count + 1
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return shareInfoList.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = shareInfoCollectionView?.dequeueReusableCell(withReuseIdentifier: "infoSelectedCell", for: indexPath) as? infoSelectedCell
        cell?.title.text = shareInfoList[indexPath.row]
        
        cell?.backgroundColor = isSelectedItemList[indexPath.row] == true ? UIColor.gray : UIColor.white
         cell?.title.backgroundColor = isSelectedItemList[indexPath.row] == true ? UIColor.gray : UIColor.white
        return cell!
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 50.0)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        isSelectedItemList[indexPath.row] = !isSelectedItemList[indexPath.row]
        shareInfoCollectionView?.reloadData()
    }
    
    @IBAction func ApplyAction(_ sender: Any) {
        debugPrint(shareInfoList)
        debugPrint(isSelectedItemList)
      //  debugPrint(shareInfoList)
    }
}
